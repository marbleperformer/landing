from django.db import models

from wagtail.wagtailcore import blocks

from modelcluster.fields import ParentalKey

from wagtail.wagtailadmin.edit_handlers import FieldPanel, FieldRowPanel, InlinePanel, MultiFieldPanel, StreamFieldPanel

from wagtail.wagtailforms.edit_handlers import FormSubmissionsPanel

from wagtail.wagtailadmin.utils import send_mail

from wagtail.wagtailimages.blocks import ImageChooserBlock

from wagtail.wagtailcore.fields import RichTextField, StreamField
from wagtail.wagtailforms.models import AbstractFormField, AbstractEmailForm

from wagtail.contrib.table_block.blocks import TableBlock as BaseTableBlock

from .filters import *

class PresentationBlock(blocks.StructBlock):
	title = blocks.CharBlock(label='Название')
	image = ImageChooserBlock(label='Изображение')
	description = blocks.RichTextBlock(label='Описание')

class InfographicBlock(blocks.StructBlock):
	image = ImageChooserBlock(label='Изображение')
	description = blocks.CharBlock(label='Описание')

class ReferenceBlock(blocks.StructBlock):
	image = ImageChooserBlock(label='Изображение')
	name = blocks.CharBlock(label='Описание')
	url = blocks.URLBlock(label='Ссылка')
	
class CarouselBlock(blocks.StructBlock):
	body = blocks.StreamBlock([
		('Таблица', BaseTableBlock()),
		('Партнеры', ReferenceBlock())
	])

class LandingBlock(blocks.StructBlock):
	title = blocks.CharBlock(label='Заголовок', required=False)
	subtitle = blocks.RichTextBlock(label='Подзаголовок', required=False)
	background = ImageChooserBlock(label='Задний фон', required=False)

class TextBlock(LandingBlock):
	content = blocks.RichTextBlock(label='Содержимое', required=False)

class TableBlock(LandingBlock):
	content = BaseTableBlock(label='Содержимое', template='blocks/base_table_block.html')

class GroupBlock(LandingBlock):
	TWO = 'col-md-6 col-sm-6 col-xs-12'
	THREE = 'col-md-4 col-sm-6 col-xs-12'
	FOUR = 'col-md-3 col-sm-6 col-xs-12'
	FIVE = 'col-md-2 col-sm-6 col-xs-12'
	COUNT_OPTIONS = (
		(TWO, 'Два'),
		( THREE, 'Три'),
		(FOUR, 'Четыре'),
		(FIVE, 'Пять')
	)
	count_class = blocks.ChoiceBlock(label='Элементов в строке', choices=COUNT_OPTIONS, default=THREE)

class FormField(AbstractFormField):
	page = ParentalKey('LandingPage', related_name='form_fields')

class LandingPage(AbstractEmailForm):
	subtitle = models.CharField(max_length=250, verbose_name='Подзаголовок', blank=True, null='True')
	logo = models.ForeignKey('wagtailimages.Image', blank=True, null='True', on_delete=models.SET_NULL)

	body = StreamField(
		[
			('richtext', TextBlock(label='Текст', template='blocks/text_block.html')),
			('contenttable', TableBlock(label='Таблица', template='blocks/table_block.html')),
			('presentations', GroupBlock(
				[
					('elements', blocks.ListBlock(PresentationBlock(template='blocks/presentation_block.html'), label='Компоненты')),
				], label='Презентация', template='blocks/group_block.html'
			)),
			('Infographic', GroupBlock(
				[
					('elements', blocks.ListBlock(InfographicBlock(template='blocks/infographic_block.html'), label='Компоненты')),
				], label='Инфографика', template='blocks/group_block.html'
			)),
			('references', GroupBlock(
				[
					('elements', blocks.ListBlock(ReferenceBlock(template='blocks/references_block.html'), label='Компоненты')),
				], label='Отсылки', template='blocks/group_block.html'
			)),
			('Карусель', LandingBlock(
				[
					('Компоненты', CarouselBlock(label='Компоненты')),
				]
			)),
		], verbose_name='Содержимое', blank=True
	)

	content_panels = [
		MultiFieldPanel([
			FieldPanel('title'),
			FieldPanel('subtitle'),
			FieldPanel('logo')
		], 'Заглавный блок'),
		StreamFieldPanel('body'),
		InlinePanel('form_fields', label='Поля формы'),
		MultiFieldPanel([
			FieldRowPanel([
				FieldPanel('from_address', classname='col6'),
				FieldPanel('to_address', classname='col6'),
			]),
			FieldPanel('subject'),
		], 'Рассылка')
	]

	@classmethod
	def can_create_at(cls, parent):
		return super(LandingPage, cls).can_create_at(parent) and not cls.objects.exists()

	class Meta:
		verbose_name = 'Главная страница'
		verbose_name_plural = 'Главную страницу'
