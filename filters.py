import jinja2
from django.forms import widgets

def add_bootstrap_class(field):
	print(field.field.widget)
	attrs = dict()
	widget_type = type(field.field.widget)
	if issubclass(widget_type, widgets.CheckboxInput):
		attrs['class'] = 'checkbox-inline'
	elif issubclass(widget_type, widgets.Input) or issubclass(widget_type, widgets.Textarea) or issubclass(widget_type, widgets.Select):
		attrs['class'] = 'form-control'
	elif issubclass(widget_type, widgets.CheckboxSelectMultiple):
		attrs['class'] = 'checkbox'
	elif issubclass(widget_type, widgets.RadioSelect):
		attrs['class'] = 'radio'
	return field.as_widget(attrs=attrs)
	
jinja2.filters.FILTERS['addbootstrapclass'] = add_bootstrap_class
